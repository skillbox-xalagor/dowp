// Copyright Epic Games, Inc. All Rights Reserved.

#include "DowpGameMode.h"
#include "Dowp/Player/DowpPlayerController.h"
#include "Dowp/Character/DowpCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADowpGameMode::ADowpGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADowpPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	if (static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Dowp/Core/Player/BP_Character"));
		PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
