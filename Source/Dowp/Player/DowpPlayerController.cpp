// Copyright Epic Games, Inc. All Rights Reserved.

#include "DowpPlayerController.h"
#include "Dowp/Character/DowpCharacter.h"

ADowpPlayerController::ADowpPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}

void ADowpPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
}
