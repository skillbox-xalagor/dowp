// Copyright Epic Games, Inc. All Rights Reserved.

#include "Dowp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Dowp, "Dowp");

DEFINE_LOG_CATEGORY(LogDowp)
