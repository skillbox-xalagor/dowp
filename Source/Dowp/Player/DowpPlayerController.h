// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "DowpPlayerController.generated.h"

UCLASS()
class ADowpPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ADowpPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;
	// End PlayerController interface
};
