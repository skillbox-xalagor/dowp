﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "RewardComponent.generated.h"

USTRUCT()
struct FReward
{
	GENERATED_BODY()

	float Score = 0;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DOWP_API URewardComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	URewardComponent();

	UPROPERTY(EditAnywhere)
	FReward Reward;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void DeadOwner();
};
