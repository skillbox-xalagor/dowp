﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "Engine/DataTable.h"
#include "Engine/GameInstance.h"
#include "DowpGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class DOWP_API UDowpGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	//Table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
	UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
