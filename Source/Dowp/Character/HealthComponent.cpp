﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;
}

float UHealthComponent::GetCurrentHealth() const
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(const float NewHealth)
{
	Health = NewHealth;
}

bool UHealthComponent::GetIsAlive() const
{
	return bIsAlive;
}

void UHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (bIsAlive)
	{
		ChangeValue = ChangeValue * CoefDamage;

		Health += ChangeValue;

		OnHealthChange.Broadcast(Health, ChangeValue);

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health <= 0.0f)
			{
				bIsAlive = false;
				OnDead.Broadcast();
			}
		}
	}
}

void UHealthComponent::DeadEvent() const
{
	OnDead.Broadcast();
}
