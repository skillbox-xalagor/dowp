﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "DowpPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class DOWP_API ADowpPlayerState : public APlayerState
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable)
	void AddScore(float ScoreDelta);
};
