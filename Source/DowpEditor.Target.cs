// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DowpEditorTarget : TargetRules
{
	public DowpEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Dowp");
	}
}