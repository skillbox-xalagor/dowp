// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "Dowp/System/Types.h"
#include "Projectile.h"
#include "Weapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadEnd, bool, bIsSuccess);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, int, CurrentRound, int, MaxRound);

UCLASS()
class DOWP_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeapon();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponReloadStart OnWeaponReloadStart;

	UPROPERTY(BlueprintAssignable, Category = "Ammo")
	FOnAmmoChange OnAmmoChange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere)
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;

	//Timers
	float FireTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;

	//Dispersion
	float Dispersion = 0.0f;

	//Timer Drop Magazine on reload
	bool DropClipFlag = false;
	float DropClipTimer = -1.0;

	//shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;
	UPROPERTY(BlueprintReadWrite)
	FVector ShootEndLocation = FVector(0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FName IdWeaponName;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Tick func End
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void WeaponInit();

	UFUNCTION()
	void Fire();
public:
	// Tick func
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);

	FORCEINLINE bool CheckWeaponCanFire() const
	{
		return !WeaponFiring;
	};

	FProjectileInfo GetProjectile();

	FORCEINLINE float GetDispersion() const
	{
		return Dispersion;
	};
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	FVector GetFireEndLocation() const;

	FORCEINLINE int8 GetNumberProjectileByShot() const
	{
		return WeaponSetting.NumberProjectileByShot;
	};

	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE int32 GetWeaponRound() const
	{
		return AdditionalWeaponInfo.Round;
	};
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE int32 GetWeaponMaxRound() const
	{
		return WeaponSetting.MaxRound;
	};

	UFUNCTION()
	void InitReload();
	void FinishReload();
	void CancelReload();

	UFUNCTION()
	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
		float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;

	UFUNCTION()
	void AnimWeaponStart(UAnimMontage* Anim);
	UFUNCTION()
	void ShellDropFire(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh,
		float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);

	UFUNCTION()
	void FXWeaponFire(UParticleSystem* FxFire, USoundBase* SoundFire);

	UFUNCTION()
	void SpawnTraceHitDecal(UMaterialInterface* DecalMaterial, FHitResult HitResult);
	UFUNCTION()
	void SpawnTraceHitFX(UParticleSystem* FxTemplate, FHitResult HitResult);
	UFUNCTION()
	void SpawnTraceHitSound(USoundBase* HitSound, FHitResult HitResult);
};
