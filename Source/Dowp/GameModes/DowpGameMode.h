// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DowpGameMode.generated.h"

UCLASS(minimalapi)
class ADowpGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADowpGameMode();
};
