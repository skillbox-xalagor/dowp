﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "RewardComponent.h"

#include "EnemyCharacter.h"
#include "HealthComponent.h"
#include "Dowp/Player/DowpPlayerState.h"
#include "GameFramework/Actor.h"


// Sets default values for this component's properties
URewardComponent::URewardComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URewardComponent::BeginPlay()
{
	Super::BeginPlay();

	UHealthComponent* HealthComp = Cast<UHealthComponent>(GetOwner()->GetComponentByClass(UHealthComponent::StaticClass()));
	HealthComp->OnDead.AddDynamic(this, &URewardComponent::DeadOwner);

}

void URewardComponent::DeadOwner()
{
	const AEnemyCharacter* EnemyCharacterOwner = Cast<AEnemyCharacter>(GetOwner());
	APlayerState* InstigatorPlayerState = EnemyCharacterOwner->GetInstigator()->GetPlayerState();
	InstigatorPlayerState->SetScore(InstigatorPlayerState->GetScore()+Reward.Score);
	DestroyComponent();
}